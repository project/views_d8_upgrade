<?php

/**
 * @file
 * Contains \Drupal\views_d8_upgrade\UpgradeViews.
 */

namespace Drupal\views_d8_upgrade;

use Symfony\Component\Yaml\Yaml;

/**
 * Defines an upgrade for the actual views, that has been used in D7.
 */
class UpgradeViews {

  /**
   * Runs the actual upgrade.
   */
  public function upgrade() {
  }

}

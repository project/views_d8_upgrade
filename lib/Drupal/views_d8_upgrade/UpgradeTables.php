<?php

/**
 * @file
 * Contains \Drupal\views_d8_upgrade\UpgradeTables.
 */

namespace Drupal\views_d8_upgrade;

use Symfony\Component\Yaml\Yaml;

/**
 * Defines an upgrade that drops the unneeded tables.
 */
class UpgradeTables {

  /**
   * Runs the actual upgrade.
   */
  public function upgrade() {
    // Drop the old tables.
    if (db_table_exists('cache_view')) {
      db_drop_table('cache_views');
    }
    if (db_table_exists('cache_views_data')) {
      db_drop_table('cache_views_data');
    }
  }

}

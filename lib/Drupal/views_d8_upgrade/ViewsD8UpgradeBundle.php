<?php

/**
 * @file
 * Contains \Drupal\views_d8_upgrade\ViewsD8UpgradeBundle.
 */

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Registers views_d8_upgrade module's services to the container.
 */
class ViewsD8UpgradeBundle extends Bundle {

  /**
   * Overrides \Symfony\Component\HttpKernel\Bundle\BundleInterface::build().
   */
  public function build(ContainerBuilder $container) {
    $container->register('plugin.manager.views_d8_upgrade.views_upgrade', 'Drupal\views_d8_upgrade\Plugin\ViewsUpgradePluginManager')
      ->addArgument('%container.namespaces%');
  }

}


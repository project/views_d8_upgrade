<?php

/**
 * @file
 * Contains \Drupal\views_d8_upgrade\UpgradeSettings.
 */

namespace Drupal\views_d8_upgrade;

use Drupal;
use Symfony\Component\Yaml\Yaml;

/**
 * Defines an upgrade that migrates the settings from d7 to d8.
 */
class UpgradeSettings {

  /**
   * The config object used for the views settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a UpgradeSettings object.
   */
  public function __construct() {
    $this->config = Drupal::service('config.factory')->get('views.settings');
  }


  /**
   * Runs the actual upgrade.
   */
  public function upgrade() {
    $yaml_file = file_get_contents('public://views_export/views.settings.yml');
    $settings = $y = Yaml::parse($yaml_file);

    $this->config->setData($settings)->save();
  }

}

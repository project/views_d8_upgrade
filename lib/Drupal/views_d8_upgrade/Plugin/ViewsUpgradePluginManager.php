<?php

/**
 * @file
 * Contains \Drupal\views_d8_upgrade\Plugin\ViewsUpgradePluginManager.
 */

namespace Drupal\views_d8_upgrade\Plugin;

use Drupal\Component\Plugin\Discovery\DerivativeDiscoveryDecorator;
use Drupal\Component\Plugin\Discovery\ProcessDecorator;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Core\Plugin\Discovery\AlterDecorator;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\CacheDecorator;

/**
 * Defines a plugin manager for views_upgrade plugins.
 */
class ViewsUpgradePluginManager extends PluginManagerBase {

  /**
   * Constructs a ViewsUpgradePluginManager object.
   *
   * @param array $namespaces
   *   An array of paths keyed by it's corresponding namespaces.
   */
  public function __construct(array $namespaces = array()) {
    $this->discovery = new AnnotatedClassDiscovery('views_d8_upgrade', 'views_upgrade', $namespaces);
    $this->discovery = new DerivativeDiscoveryDecorator($this->discovery);
    $this->discovery = new ProcessDecorator($this->discovery, array($this, 'processDefinition'));
    $this->discovery = new AlterDecorator($this->discovery, 'views_d8_upgrade_views_upgrade');
    $this->discovery = new CacheDecorator($this->discovery, 'cache');

    $this->factory = new DefaultFactory($this->discovery);

    $this->defaults += array(
      'parent' => 'parent',
      'plugin_type' => 'views_upgrade',
      'module' => 'views_d8_uprade',
      'register_theme' => TRUE,
    );
  }

}

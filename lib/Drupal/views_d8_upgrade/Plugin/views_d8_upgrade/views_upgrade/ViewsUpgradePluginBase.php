<?php

/**
 * @file
 * Contains \Drupal\views_d8_upgrade\Plugin\views_d8_upgrade\views_upgrade\ViewsUpgradePluginBase.
 */

namespace Drupal\views_d8_upgrade\Plugin\views_d8_upgrade\views_upgrade;

use Drupal\views\Plugin\Core\Entity\View;

/**
 * Defines an abstract base class for views upgrade plugins.
 *
 * @todo Should there be an interface?
 */
abstract class ViewsUpgradePluginBase {

  /**
   * Runs a certain upgrade on a view.
   */
  abstract public function upgradeView(View $view);

}

<?php

/**
 * @file
 * Contains \Drupal\views_d8_upgrade\Plugin\views_d8_upgrade\views_upgrade\BasicDisplayUpgrade.
 */

namespace Drupal\views_d8_upgrade\Plugin\views_d8_upgrade\views_upgrade;

use Drupal\views\Plugin\Core\Entity\View;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Upgrades the display properties like:
 *   -
 * @Plugin(
 *   id = "basic_display",
 *   label = @Translation("Basic display"),
 *   module = "views_d8_upgrade",
 * )
 */
class BasicPropertiesDisplay extends ViewsUpgradePluginBase {

  /**
   * Runs a certain upgrade on a view.
   */
  public function upgradeView(View $view) {
    $view->set('id', $view->get('name'));
    $view->set('name', NULL);

    $view->set('vid', NULL);
    $view->set('core', NULL);

    $view->set('label', $view->get('human_name'));
    $view->set('human_name', NULL);

    $view->set('langcode', NULL);
  }

}
